/**
 * These materials are confidential and proprietary to Intellect Design Arena Ltd. and no part of these
 * materials should be reproduced, published, transmitted or distributed in any form or by any means,
 * electronic, mechanical, photocopying, recording or otherwise, or stored in any information storage or
 * retrieval system of any nature nor should the materials be disclosed to third parties or used in any
 * other manner for which this is not authorized, without the prior express written authorization of
 * Intellect Design Arena Ltd.
 *
 * Copyright 2019 Intellect Design Arena Limited. All rights reserved.
 *
 */
/*********************************************************************
 *            Version Control Block
 *
 * Date        Version		Author               Description
 * ---------   --------	 ---------------  ---------------------------
 * 20-Aug-2019	 1.0	 	 Ashish Powar      
 *********************************************************************/

package com.intellect.contextual.auditconsumer.dao;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;

@PropertySource(value = { "classpath:database.properties" })
@Slf4j
@Service
public class DataSourceConfiguration
{
	@Autowired
	private Environment env;

	public DataSource getDataSource(String strDSName)
	{

		DataSourceBuilder dataSourceBuilder = DataSourceBuilder.create();
		log.info("class : {}", env.getProperty("DB_DRIVER_CLASS_LPS"));
		dataSourceBuilder.driverClassName(env.getProperty("DB_DRIVER_CLASS_LPS"));
		dataSourceBuilder.url(env.getProperty("DB_URL_LPS"));
		dataSourceBuilder.username(env.getProperty("DB_USERNAME_LPS"));
		dataSourceBuilder.password(env.getProperty("DB_PASSWORD_LPS"));
		return dataSourceBuilder.build();

	}
	
	public JdbcTemplate getJDBCTemplate(String strDSName)
	{
		DataSource ds =getDataSource(strDSName);
		return new JdbcTemplate(ds);
		
	}

}
